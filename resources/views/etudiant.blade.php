@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    <div class="alert alert-warning" role="alert">
                        <p>
                            Hi there, awesome Etudiant
                        </p>
                        A simple warning alert with .... Give it a click if you like.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
